import test from 'ava'
import {Scope} from './scope.js'
import {scope_eval_string} from './runtime.js'

let program = async (t, src, {
  return: expected_return,
  stdout: expected_stdout,
  stderr: expected_stderr,
})=>{
  let stdout = ''
  let stderr = ''
  let fake_stream = write=>({write})
  let scope = new Scope()
  scope.set('stdout', fake_stream(msg=>stdout += msg))
  scope.set('stderr', fake_stream(msg=>stderr += msg))
  let return_value = await scope_eval_string(scope, src)
  t.deepEqual(return_value, expected_return)
  t.deepEqual(stdout, expected_stdout||'')
  t.deepEqual(stderr, expected_stderr||'')
}

test('hello world', program, `
  (println "Hello world")
`, {stdout: 'Hello world\n'})

test('simple math', program, `
  (- (+ 10 10 6) 33 22 11)
`, {return: -40})

test('variables', program, `
  (let foo "bar")
  foo
`, {return: 'bar'})

test('functions', program, `
  (let add (fn (a b) (+ a b)))
  (add 1 2)
`, {return: 3})

// TODO josh: support shortcut for define function and define macro
// - function defn deffn
// - defmacro
