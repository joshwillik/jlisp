import {Macro} from './types.js'
import {scope_eval} from './runtime.js'

let builtins = {}
let define = (name, {build})=>{
  builtins[name] = {build}
}

export const build_scope = context=>{
  let scope_values = {}
  for (let [key, {build}] of Object.entries(builtins))
    scope_values[key] = build(context)
  return scope_values
}

define('let', {
  build: ({scope})=>new Macro(async ([name, value])=>{
    scope.set(name, await scope_eval(scope, value))
  })
})

let function_body = context=>async (...in_forms)=>{
  let {scope, args, body_forms} = context
  for (let i = 0; i<args.length; i++)
    scope.set(args[i], in_forms[i])
  let last
  for (let form of body_forms)
    last = await scope_eval(scope, form)
  return last
}

define('macro', {
  build: ({scope})=>([args, ...body_forms])=>{
    return new Macro(function_body({scope: scope.child(), args, body_forms}))
  },
})

define('fn', {
  build: ({scope})=>new Macro(([args, ...body_forms])=>{
    return function_body({scope: scope.child(), args, body_forms})
  }),
})

define('println', {
  build: ({scope})=>(...args)=>{
    scope.lookup('stdout').write(args.join(' ')+'\n')
  },
})
define('+', {
  build: ()=>(...args)=>args.reduce((a, b)=>a+b, 0),
})
define('*', {
  build: ()=>(...args)=>args.reduce((a, b)=>a*b, 1),
})
define('-', {
  build: ()=>(...args)=>args.slice(1).reduce((a, b)=>a-b, args[0]),
})
define('/', {
  build: ()=>(...args)=>args.slice(1).reduce((a, b)=>a/b, args[0]),
})
