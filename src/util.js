export async function map_async(values, fn){
  let out = []
  for (let v of values)
    out.push(await fn(v))
  return out
}

export function partition(values, filter) {
  let left = []
  let right = []
  for (let v of values) {
    if (filter(v))
      left.push(v)
    else
      right.push(v)
  }
  return [left, right]
}
