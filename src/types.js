export class Identifier {
  constructor(value) { this.value = value }
  toString(){ return this.value }
}

export class Macro {
  constructor(value) { this.value = value }
  toString() { return `macro` }
}
