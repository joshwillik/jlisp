import test from 'ava'
import {parse} from './parser.js'
import {Identifier} from './types.js'

let parse_program = (src, expected)=>test(`parse ${src}`, t=>{
  t.deepEqual(parse(src), expected)
})

parse_program(`"Hi there"`, ['Hi there'])
parse_program(`log`, [new Identifier('log')])
parse_program(`1`, [1])
parse_program(`1 2`, [1, 2])
parse_program(`(1 2)`, [
  [1, 2],
])
parse_program(`()`, [[]])
parse_program(`(log "hello world")`, [
  [
    new Identifier('log'),
    'hello world',
  ]
])
parse_program(`(fn (arg1 arg2) (concat arg1 " " arg2 "!"))`, [
  [
    new Identifier('fn'),
    [
      new Identifier('arg1'),
      new Identifier('arg2'),
    ],
    [
      new Identifier('concat'),
      new Identifier('arg1'),
      ' ',
      new Identifier('arg2'),
      '!',
    ],
  ],
])
