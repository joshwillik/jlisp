/* eslint-disable no-console */
const DEBUG = process.env.DEBUG

export function debug(...msg) {
  if (DEBUG)
    console.error(...msg)
}
