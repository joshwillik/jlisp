import {Identifier} from './types.js'
const ALPHABET = 'abcdefghijklmnopqrstuvwxyz'
const DIGITS = '1234567890'
const ALPHANUM = [...ALPHABET, ...ALPHABET.toUpperCase(), ...DIGITS]
const IDENTIFER_SYMBOLS = '_-+=<>~'

const tokens = {
  string: v=>({type: 'string', value: v}),
  number: v=>({type: 'number', value: v}),
  identifier: v=>({type: 'identifier', value: v}),
  list: v=>({type: 'list', value: v}),
  white: v=>({type: 'white', value: v}),
}

export function parse(src_string){
  let v = many(expression)(src_string)
  if (v)
    return v[0].filter(t=>t.type!='white').map(collapse)
}

function collapse(token){
  let value = token.value
  if (token.type=='list')
    return value.map(collapse)
  return value
}

const whitespace = transform(take_while1(c=>'\t\n '.includes(c)),
  v=>tokens.white(v.join('')))

const string = transform(delimited('"'), tokens.string)

const optional = fn=>{
  return src=>{
    return fn(src) || [undefined, src]
  }
}

// TODO josh: should error on numbers with multiple decimal places
const number = transform(and(
  optional(or(lit('-'), lit('+'))),
  transform(take_while1(c=>'1234567890.'.includes(c)),
    v=>v.join('')),
), v=>tokens.number(+v.join('')))

const identifier = transform(take_while1(one_of([...ALPHANUM, ...IDENTIFER_SYMBOLS])),
  v=>tokens.identifier(new Identifier(v.join(''))))

const list = transform(and(
  lit('('),
  many(src=>expression(src)),
  lit(')'),
), v=>tokens.list(v[1].filter(t=>t.type!='white')))

const expression = or(
  whitespace,
  string,
  number,
  identifier,
  list,
)

function lit(expected) {
  return str=>{
    if (str.startsWith(expected))
      return [expected, str.slice(expected.length)]
  }
}

function one_of(chars) {
  return c=>chars.includes(c)
}

function or(...fns) {
  return src=>{
    for (let fn of fns) {
      let v = fn(src)
      if (v)
        return v
    }
  }
}

function delimited(delimiter) {
  return str=>{
    let v = and(
      lit(delimiter),
      transform(take_while(not(is('"'))), v=>v.join('')),
      lit(delimiter)
    )(str)
    if (v)
      return [v[0][1], v[1]]
  }
}

function is(match) {
  return v=>v==match
}

function not(fn) {
  return v=>!fn(v)
}

function and(...fns) {
  return str=>{
    let matches = []
    for (let fn of fns) {
      let maybe = fn(str)
      if (!maybe)
        return
      matches.push(maybe[0])
      str = maybe[1]
    }
    return [matches, str]
  }
}

function many(fn) {
  return str=>{
    let matches = []
    while (true) { // eslint-disable-line no-constant-condition
      let v = fn(str)
      if (v) {
        matches.push(v[0])
        str = v[1]
      } else {
        break
      }
    }
    return [matches, str]
  }
}

function take_while(test) {
  return str=>{
    let matches = []
    let i = 0
    for (; i<str.length; i++){
      if (test(str[i])) {
        matches.push(str[i])
      } else {
        break
      }
    }
    return [matches, str.slice(i)]
  }
}
function take_while1(test) {
  return str=>{
    let matches = []
    let i = 0
    for (; i<str.length; i++){
      if (test(str[i])) {
        matches.push(str[i])
      } else {
        break
      }
    }
    if (matches.length)
      return [matches, str.slice(i)]
  }
}

function transform(fn, then) {
  return str=>{
    let v = fn(str)
    if (v)
      return [then(v[0]), v[1]]
  }
}

export const test = {tokens, list, number}
