import test from 'ava'
import {test as parser_t} from './parser.js'
const {list, tokens} = parser_t

let parse_list = (src, token, remainder)=>test(src, t=>{
  let res = list(src)
  t.deepEqual(res, [token, remainder])
})

parse_list('(1 2)', tokens.list([
  tokens.number(1),
  tokens.number(2),
]), '')
