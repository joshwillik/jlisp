import test from 'ava'
import {test as parser_t} from './parser.js'
const {number, tokens} = parser_t

let parse_number = (src, token, remainder)=>test(src, t=>{
  let res = number(src)
  t.deepEqual(res, [token, remainder||''])
})

parse_number('1', tokens.number(1))
parse_number('11', tokens.number(11))
parse_number('-11', tokens.number(-11))
parse_number('11.2', tokens.number(11.2))
parse_number('-11.2', tokens.number(-11.2))
parse_number('+11.2', tokens.number(11.2))
