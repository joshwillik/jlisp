/* eslint-disable no-console */
import readline from 'readline'
import {scope_eval_string} from './runtime.js'
import {Scope} from './scope.js'
import {partition} from './util.js'
import fs from 'fs'

async function main(){
  let [flags, argv] = partition(process.argv.slice(2),
    f=>f.startsWith('-') || f.startsWith('--'))
  let [input_file] = argv
  flags = flags.reduce((obj, flag)=>{
    obj[flag] = true
    return obj
  }, {})
  let scope = new Scope()
  scope.set('stdout', process.stdout)
  if (input_file) {
    let src = fs.readFileSync(input_file)
    await scope_eval_string(scope, src)
    if (typeof scope.lookup('main')=='function')
      await scope_eval_string(scope, '(main)')
  }
  if (flags['-r'])
    await repl(scope)
}

async function repl(scope) {
  for await (let line of reader()) {
    try {
      let result = await scope_eval_string(scope, line)
      console.log(result)
    } catch (e) {
      console.error(e)
    }
  }
}

const reader = ()=>readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  prompt: '> ',
})

main().catch(e=>{
  console.error(e)
  process.exit(1)
})
