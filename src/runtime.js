import {parse} from './parser.js'
import {Identifier, Macro} from './types.js'
import {ERR_UNDEFINED} from './errors.js'
import {map_async} from './util.js'
import {debug} from './debug.js'

export const scope_eval_string = async (scope, src)=>{
  let value
  for (let expression of parse(src))
    value = await scope_eval(scope, expression)
  return value
}
export async function scope_eval(scope, ast) {
  log(`eval`, ast)
  if (!Array.isArray(ast))
    return await resolve_ast(scope, ast)
  if (!ast.length)
    return ast
  let [first, ...rest] = ast
  let first_value = await scope_eval(scope, first)
  let tag = first_value.name
  if (first_value instanceof Macro) {
    log(`${tag} macro call`, rest)
    let new_ast = await first_value.value(rest)
    log(`${tag} macro result`, new_ast)
    return new_ast
  }
  if (typeof first_value == 'function') {
    let args = await map_async(rest, v=>scope_eval(scope, v))
    log(`${tag} function call`, args)
    let retval = await first_value(...args)
    log(`${tag} function result`, retval)
    return retval
  }
  return [first_value, ...(await map_async(rest, v=>scope_eval(v)))]
}

function log(...args){ debug('[runtime]', ...args) }

async function resolve_ast(scope, ast) {
  if (ast instanceof Identifier) {
    let v = scope.lookup(ast)
    if (v==ERR_UNDEFINED)
      throw new Error(`'${ast}' is undefined`)
    return v
  }
  if (Array.isArray(ast))
    return await map_async(ast, form=>scope_eval(scope, form))
  return ast
}
