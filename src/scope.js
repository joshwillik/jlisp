import {debug} from './debug.js'
import {build_scope} from './builtins.js'
import {Identifier} from './types.js'
import {ERR_UNDEFINED} from './errors.js'

const normalize = k=>k instanceof Identifier ? k.value : k

export class Scope {
  constructor({parent, root} = {}) {
    this.symbols = new Map()
    this.parent = parent
    this.root = root||this
    this._counter = 0
    this.id = this.root.scope_id()
    let globals = build_scope({
      scope: this,
    })
    for (let [k, v] of Object.entries(globals))
      this.symbols.set(k,  v)
  }
  scope_id(){ return this._counter++ }
  has(key) { return this.symbols.has(normalize(key)) }
  get(key) { return this.symbols.get(normalize(key))}
  set(k, v){
    this.debug('set', k, v)
    this.symbols.set(normalize(k), v)
  }
  lookup(key) {
    this.debug('lookup', key)
    if (this.has(key)) {
      let v = this.get(key)
      this.debug('lookup resolved', key, v)
      return v
    }
    if (this.parent)
      return this.parent.lookup(key)
    this.debug('lookup resolved', key, ERR_UNDEFINED)
    return ERR_UNDEFINED
  }
  child() { return new Scope({parent: this}) }
  debug(...msg){ debug(`[scope:${this.id}]`, ...msg) }
}
